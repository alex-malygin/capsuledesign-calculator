const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

const src = path.resolve(__dirname, '../src');
const dist = path.resolve(__dirname, '../dist');

module.exports = {
    entry: {
        app: src + '/client'
    },
    output: {
        filename: '[name].js',
        path: dist,
        publicPath: '/',
    },
    resolve: {
        extensions: [ '.ts', '.tsx', '.json', '.css', '.scss', '.htm', '.html', '.js' ],
        modules: [ 'src', 'node_modules' ],
    },
    devtool: 'module-inline-source-map',
    devServer: {
        contentBase: dist,
        historyApiFallback: true,
        host: "0.0.0.0",
        port: 3000
    },
    module: {
        rules: [
            { enforce: 'pre', test: /\.tsx?$/, exclude: /node_modules/, use: [ 'tslint-loader' ]},
            { test: /\.tsx?$/, exclude: /node_modules/, use: [ 'ts-loader' ]},
            { test: /\.(ttf|woff2?|eot|pdf|svg)(\?.*)?$/, use: [ 'file-loader' ]},
            { test: /\.(png|jpe?g|gif)(\?.*)?$/, use: [
                { loader: 'url-loader', options: { limit: 8182 }}
            ]},
            { test: /\.s?css$/, use: [ 'style-loader', 'css-loader', 'postcss-loader', 'sass-loader' ]}

        ]
    },
    plugins: [
        new HtmlPlugin({
            inject: true,
            filename: 'index.html',
            template: src + '/assets/index',
        }),
        new webpack.NoEmitOnErrorsPlugin(),
    ],
    target: 'web'
}