import Typography from "@material-ui/core/Typography";
import React, { Component } from "react";

import { withStyles, WithStyles } from "client/ui";

import { CapsulesVariantsComponent } from "./CapsulesVariantsComponent";
import { OptionsComponent } from "./OptionsComponent";

const styles = () => ({
    title: {
        marginBottom: 60,
        marginTop: 100,
        textAlign: "center",
    },
});

interface IDesignProps extends WithStyles<typeof styles> {}

@withStyles(styles)
export class DesignComponent extends Component<IDesignProps> {
    public render() {
        return (
            <>
                <Typography
                    className={this.props.classes.title}
                    variant="title"
                >
                    Посмотреть капсулу
                </Typography>
                <CapsulesVariantsComponent />
                <OptionsComponent />
            </>
        );
    }
}
