import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";
import React, { Component } from "react";

import { Capsule, Option } from "client/models";
import { UserInfoStore } from "client/stores";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    button: {
        backgroundColor: "#e64a4a",
        borderRadius: 56,
        color: "white",
        margin: "116px 0 74px",
        padding: "16px 50px",
    },
    buttonText: {
        fontSize: 26,
    },
    buttonWrapper: {
        textAlign: "center",
    },
    column: {
        display: "flex",
        flexDirection: "column",
    },
    option: {
        ["&:last-child"]: {
            marginBottom: 0,
        },
        display: "flex",
        marginBottom: 24,
        textTransform: "uppercase",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    row: {
        display: "flex",
        flexDirection: "row",
        marginBottom: 44,
    },
    wrapper: {
        marginRight: 24,
        width: "20%",
    },
});

interface IPackageOptionsProps extends WithStyles<typeof styles> {
    capsule: Capsule;
    options: Option[];
    selected: string[];
    userInfoStore: UserInfoStore;
    onSelect: (val: string) => void;
}

//  tslint:disable-next-line array-type
function chunk<T>(arr: T[], chunkSize: number): Array<Array<T>> {
    return arr.reduce((prevVal: any, currVal: any, currIndx: number, array: T[]) =>
        !(currIndx % chunkSize) ?
        prevVal.concat([array.slice(currIndx, currIndx + chunkSize)]) :
        prevVal, []);
}
@withStyles(styles)
export class PackageOptionsComponent extends Component<IPackageOptionsProps> {
    public render() {
        const { classes, options } = this.props;
        const opts = chunk<Option>(options, 2);
        return (
            <div>
                <Typography variant="title">Заказать дополнительную услугу «комплектация»:</Typography>
                <div className={classes.row}>
                    {opts.map(this.renderWrapper)}
                </div>
                <Typography>*Cтоимость услуги «комплектация» узнавайте у вашего менеджера.</Typography>
                <div className={classes.buttonWrapper}>
                    <Button className={classes.button} onClick={this.onClick}>
                        <Typography color="inherit" className={classes.buttonText}>Заказать дизайн капсулы</Typography>
                    </Button>
                </div>
            </div>
        );
    }

    private onClick = () => {
        const { capsule, userInfoStore } = this.props;
        const url = "https://mandrillapp.com/api/1.0/messages/send.json";
        const body = JSON.stringify({
            key: "rJr75q2wVFHbUunhbSELtQ",
            message: {
                autotext: true,
                from_email: "baza@capsuledesign.ru",
                html: userInfoStore.userInfo.toString() + capsule.toString(),
                subject: "Заказать капсулу",
                to: [{
                    email: "baza@capsuledesign.ru",
                    type: "to",
                }],
            },
        });
        // @ts-ignore
        fetch(url, { method: "POST", mode: "cors", body }).then((res) => console.debug({ res }));
    }

    private renderWrapper = (options: Option[], index: number) => {
        const { classes } = this.props;
        return (
            <div key={index} className={classNames(classes.column, classes.wrapper)}>
                {options.map(this.renderOption)}
            </div>
        );
    }

    private renderOption = (option: Option) => {
        const { classes, selected } = this.props;

        return (
            <div
                key={option.value}
                className={classes.option}
            >
                <Checkbox
                    value={option.value}
                    className={classes.radioClass}
                    checked={selected.indexOf(option.value) >= 0}
                    classes={{ root: classes.radio }}
                    onChange={this.onOptionSelect}
                    icon={<RadioButtonIcon checked={false} />}
                    checkedIcon={<RadioButtonIcon checked={true} />}
                />
                <Typography>{option.label}</Typography>
            </div>
        );
    }

    private onOptionSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onSelect(e.target.value);
    }
}
